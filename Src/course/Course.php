<?php

namespace BitmCourseApp\course;
use PDO;
include_once (dirname(__FILE__) . '/../DBConnection/DBConnection.php');

class Course extends \DBConnection {
    public $id = "";
    public $uniq_id = "";
    public $course_name = "";
    public $course_description = "";
    public $course_duration = "";
    public $course_type = "";
    public $course_fee = "";
    public $course_offer = "";
    public $data = "";
	public $created;
	public $modified;
	public $deleted;
	public $error;
    
    public function prepare($data = ""){
		if (!empty($data['id'])) {
            $this->id = $data['id'];
        }
        if (!empty($data['title'])) {
            $this->course_name = $data['title'];
        }
        if (!empty($data['duration'])) {
            $this->course_duration = $data['duration'];
        }
        if (!empty($data['course_type'])) {
            $this->course_type = $data['course_type'];
        } 
        if (!empty($data['course_fee'])) {
            $this->course_fee = $data['course_fee'];
        }elseif ( $this->course_type == 'free') {
        	$this->course_fee = 0;
        }else {
            $this->course_fee = 0;
        }
        if (!empty($data['is_offer'])) {
            $this->course_offer = $data['is_offer'];
        }else{
        	$this->course_offer = '0';
        }
        if (!empty($data['description'])) {
            $this->course_description = $data['description'];
        }

		return $this;
	}
}
