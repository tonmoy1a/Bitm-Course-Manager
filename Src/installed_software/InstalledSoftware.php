<?php

namespace BitmCourseApp\installed_software;

use PDO;

include_once (dirname(__FILE__) . '/../DBConnection/DBConnection.php');

class InstalledSoftware extends \DBConnection {

    public $id, $labno, $software_title, $software_version, $software_type, $data, $action, $viewBy, $error;

    public function prepare($data = '') {
        if (!empty($data['id'])) {
            $this->id = $data['id'];
        }
        if (!empty($data['lab_id'])) {
            $this->labno = $data['lab_id'];
        }
        if (!empty($data['software_title'])) {
            $this->software_title = $data['software_title'];
        }
        if (!empty($data['software_version'])) {
            $this->software_version = $data['software_version'];
        }
        if (!empty($data['software_type'])) {
            $this->software_type = $data['software_type'];
        }
        if (!empty($data['viewBy'])) {
            $this->viewBy = $data['viewBy'];
        }
        if (!empty($data['action'])) {
            $this->action = $data['action'];
        }
    }

    public function sessionMessage($session_key = '') {
        if (!empty($_SESSION[$session_key]) && isset($_SESSION[$session_key])) {
            echo $_SESSION[$session_key];
            unset($_SESSION[$session_key]);
        }
    }

    public function validation() {
        $required = array(
            'Lab_no' => $this->labno,
            'Software_Title' => $this->software_title,
            'Software_Version' => $this->software_version,
            'Software_Type' => $this->software_type,
        );

        foreach ($required as $key => $field) {
            if (empty($field)) {
                $_SESSION["$key"] = str_replace('_', ' ', $key) . ' Required';
                $this->error = TRUE;
            }
        }

        if ($this->error == TRUE) {
            $input_value = array(
                'Lab_no' => $this->labno,
                'Software_Title' => $this->software_title,
                'Software_Version' => $this->software_version,
                'Software_Type' => $this->software_type,
            );

            foreach ($input_value as $key => $i_value) {
                if (!empty($i_value)) {
                    $_SESSION["v_$key"] = $i_value;
                }
            }
        }
    }

    public function store() {
        if ($this->error == FALSE) {
            try {
                $query = "INSERT INTO `installed_softwares` (`id`, `labinfo_id`, `software_title`, `version`, `software_type`, `is_delete`, `created`, `updated`, `deleted`) VALUES (NULL, :labid , :software_title , :version , :type , :is_delete , :created , :updated , :deleted )";
                $stmt = $this->conn->prepare($query);
                $stmt->execute(array(
                    ':labid' => $this->labno,
                    ':software_title' => $this->software_title,
                    ':version' => $this->software_version,
                    ':type' => $this->software_type,
                    ':is_delete' => '0',
                    ':created' => date("Y-m-d h:i:s"),
                    ':updated' => '',
                    ':deleted' => '',
                ));
                $_SESSION['success'] = "New Software Added";
            } catch (PDOException $e) {
                
            }
        }
        header('location:add_new_software.php');
    }

    public function viewAllSoftware($view_by = '') {
        try {
            if ($view_by['viewBy'] == 'running') {
                $vq = "SELECT * FROM `installed_softwares` WHERE is_delete=0 ORDER BY `id` DESC ";
            } else {
                $vq = "SELECT * FROM `installed_softwares` WHERE is_delete=1 ORDER BY `id` DESC ";
            }
            $view_query = $this->conn->prepare($vq);
            $view_query->execute();
            while ($row = $view_query->fetch(PDO::FETCH_ASSOC)) {
                $this->data[] = $row;
            }
            return $this->data;
        } catch (Exception $ex) {
            
        }
    }

    public function viewSoftware() {
        try {
            $query = $this->conn->query("SELECT * FROM installed_softwares WHERE id='$this->id' ");
            $row = $query->fetch();
            return $row;
        } catch (PDOException $ex) {
            
        }
    }
    
    public function labInfo() {
        try {
            $vq = "SELECT * FROM `labinfo` ";
            $view_query = $this->conn->prepare($vq);
            $view_query->execute();
            while ($row = $view_query->fetch(PDO::FETCH_ASSOC)) {
                $lab_info[] = $row;
            }
            return $lab_info;
        } catch (Exception $ex) {
            
        }
    }

    public function updateSoftware() {
        if ($this->error == FALSE) {
            try {
                $query = "UPDATE `installed_softwares` SET `labinfo_id` = :lab_id, `software_title` = :software_title, `version` = :version, `software_type` = :software_type, `updated` = :updated WHERE `installed_softwares`.`id` = $this->id ";
                $stmt = $this->conn->prepare($query);
                $stmt->execute(array(
                    ':lab_id' => $this->labno,
                    ':software_title' => $this->software_title,
                    ':version' => $this->software_version,
                    ':software_type' => $this->software_type,
                    ':updated' => date("Y-m-d h:i:s"),
                ));

                $_SESSION['success'] = "Lab Information Updated Successfully.";
                header("location:edit_software.php?id=$this->id");
            } catch (PDOException $e) {
                echo 'Error: ' . $e->getMessage();
            }
        } else {
            header("location:edit_software.php?id=$this->id");
        }
    }

    public function deleteSoftware() {
        try {
            if ($this->action == 'disable') {
                $query = "UPDATE `installed_softwares` SET `is_delete` = '1', `deleted` = :date WHERE `installed_softwares`.`id` = $this->id";
            } elseif ($this->action == 'restore') {
                $query = "UPDATE `installed_softwares` SET `is_delete` = '0', `deleted` = :date WHERE `installed_softwares`.`id` = $this->id";
            } elseif ($this->action == 'delete') {
                $query = "DELETE FROM `installed_softwares` WHERE `installed_softwares`.`id` = $this->id ";
            }
            $stmt = $this->conn->prepare($query);
            $stmt->execute(array(
                ':date' => date("Y-m-d h:i:s")
            ));
            header("location:view_software.php?viewBy=$this->viewBy");
        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }

}

?>
