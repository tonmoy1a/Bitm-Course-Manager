<?php
include '../../vendor/autoload.php';
use BitmCourseApp\lab_info\LabInfo;

$labobj = new LabInfo();

$labobj->prepare($_POST);
$labobj->validation();
$labobj->updateLab();
?>