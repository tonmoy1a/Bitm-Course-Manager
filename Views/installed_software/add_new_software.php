<?php
include '../../vendor/autoload.php';

use BitmCourseApp\user\user_login\UserLogin;
use BitmCourseApp\installed_software\InstalledSoftware;

$objlogin = new UserLogin();
$objsoft = new InstalledSoftware();

$loged_user = $objlogin->checkLogin();
?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>BITM Course Manager</title>

<!-- Global stylesheets -->
<link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
<link rel="shortcut icon" type="image/png" href="../elements/bitm-icon.png"/>
<link href="../assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
<link href="../assets/css/minified/bootstrap.min.css" rel="stylesheet" type="text/css">
<link href="../assets/css/minified/core.min.css" rel="stylesheet" type="text/css">
<link href="../assets/css/minified/components.min.css" rel="stylesheet" type="text/css">
<link href="../assets/css/minified/colors.min.css" rel="stylesheet" type="text/css">
<!-- /global stylesheets -->

<!-- Core JS files -->
<script type="text/javascript" src="../assets/js/plugins/loaders/pace.min.js"></script>
<script type="text/javascript" src="../assets/js/core/libraries/jquery.min.js"></script>
<script type="text/javascript" src="../assets/js/core/libraries/bootstrap.min.js"></script>
<script type="text/javascript" src="../assets/js/plugins/loaders/blockui.min.js"></script>
<!-- /core JS files -->

<!-- Theme JS files -->
<script type="text/javascript" src="../assets/js/plugins/visualization/d3/d3.min.js"></script>
<script type="text/javascript" src="../assets/js/plugins/visualization/d3/d3_tooltip.js"></script>
<script type="text/javascript" src="../assets/js/plugins/forms/styling/switchery.min.js"></script>
<script type="text/javascript" src="../assets/js/plugins/forms/styling/uniform.min.js"></script>
<script type="text/javascript" src="../assets/js/plugins/forms/selects/bootstrap_multiselect.js"></script>
<script type="text/javascript" src="../assets/js/plugins/ui/moment/moment.min.js"></script>
<script type="text/javascript" src="../assets/js/plugins/pickers/daterangepicker.js"></script>
<script type="text/javascript" src="../assets/js/plugins/forms/selects/select2.min.js"></script>
<script type="text/javascript" src="../assets/js/pages/form_layouts.js"></script>

<script type="text/javascript" src="../assets/js/core/app.js"></script>
<script type="text/javascript" src="../assets/js/pages/dashboard.js"></script>
<!-- /theme JS files -->
</head>

<body>
<!-- Main navbar -->
<div class="navbar navbar-inverse">
    <div class="navbar-header">
        <a class="navbar-brand" href="../dashboard.php"><img src="../assets/images/logo_light.png" alt=""></a>

        <ul class="nav navbar-nav visible-xs-block">
            <li><a data-toggle="collapse" data-target="#navbar-mobile"><i class="icon-tree5"></i></a></li>
            <li><a class="sidebar-mobile-main-toggle"><i class="icon-paragraph-justify3"></i></a></li>
        </ul>
    </div>

    <div class="navbar-collapse collapse" id="navbar-mobile">
        <ul class="nav navbar-nav">
            <li><a class="sidebar-control sidebar-main-toggle hidden-xs"><i class="icon-paragraph-justify3"></i></a></li>
        </ul>

        <ul class="nav navbar-nav navbar-right">
            <li class="dropdown dropdown-user">
                <a class="dropdown-toggle" data-toggle="dropdown">
                    <img src="../elements/user_images/<?php echo $loged_user['image']; ?>" width="28" height="28" alt="">
                    <span><?php echo $loged_user['username']; ?></span>
                    <i class="caret"></i>
                </a>

                <ul class="dropdown-menu dropdown-menu-right">
                    <li><a href="../user/user_view_single.php?id=<?php echo $loged_user['id']; ?>"><i class="icon-user-plus"></i> My profile</a></li>
                    <li class="divider"></li>
                    <li><a href="#"><i class="icon-cog5"></i> Account settings</a></li>
                    <li><a href="../logout.php"><i class="icon-switch2"></i> Logout</a></li>
                </ul>
            </li>
        </ul>
    </div>
</div>
<!-- /main navbar -->


<!-- Page container -->
<div class="page-container">

    <!-- Page content -->
    <div class="page-content">

        <!-- Main sidebar -->
        <div class="sidebar sidebar-main">
            <div class="sidebar-content">

                <!-- User menu -->
                <div class="sidebar-user">
                    <div class="category-content">
                        <div class="media">
                            <a href="#" class="media-left"><img src="../elements/user_images/<?php echo $loged_user['image']; ?>" class="img-circle img-sm" alt=""></a>
                            <div class="media-body">
                                <span class="media-heading text-semibold"><?php echo $loged_user['full_name']; ?></span>
                                <div class="text-size-mini text-muted">

                                </div>
                            </div>

                            <div class="media-right media-middle">
                                <ul class="icons-list">
                                    <li>
                                        <a href="#"><i class="icon-cog3"></i></a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /user menu -->

                <!-- Main navigation -->
                <div class="sidebar-category sidebar-category-visible">
                    <div class="category-content no-padding">
                        <ul class="navigation navigation-main navigation-accordion">
                            <!-- Main -->
                            <li class="navigation-header"><span>Main</span> <i class="icon-menu" title="Main pages"></i></li>
                            <li><a href="../dashboard.php"><i class="icon-home4"></i> <span>Dashboard</span></a></li>
                            <li><a href="../user/user_manager.php"><i class="icon-user"></i> <span>User Menu</span></a></li>
                            <li><a href="../trainer/trainer.php"><i class="icon-people"></i> <span>Trainer</span></a></li>
                            <li><a href="../course/course.php"><i class="icon-book"></i> <span>Course</span></a></li>
                            <li><a href="../lab_info/lab_info.php"><i class="icon-lan"></i> <span>Lab Info</span></a></li>
                            <li class="active"><a href="software_info.php"><i class="icon-laptop"></i> <span>Installed Software</span></a></li>
                            <li><a href="../assign_course/assign_course.php"><i class="icon-list-unordered"></i> <span>Assign Course</span></a></li>
                        </ul>
                    </div>
                </div>
                <!-- /main navigation -->
            </div>
        </div>
        <!-- /main sidebar -->
        <!-- Main content -->
        <div class="content-wrapper">
            <!-- Page header -->
            <div class="page-header">
                <div class="page-header-content">
                    <div class="page-title">
                        <h4><i class="icon-lan"></i> - <span class="text-semibold">Installed Software</span></h4>
                    </div>
                </div>

                <div class="breadcrumb-line">
                    <ul class="breadcrumb">
                        <li><a href="../dashboard.php"><i class="icon-home2 position-left"></i> Dashboard</a></li>
                        <li><a href="software_info.php">Installed Software</a></li>
                        <li class="active">Add New Software</li>
                    </ul>
                </div>

            </div>
            <!-- /page header -->
            <!-- Content area -->
            <div class="content">
                <!-- Dashboard content -->
                    <div class="panel panel-flat">
                        <div class="navbar navbar-default navbar-xs">
                            <ul class="nav navbar-nav no-border visible-xs-block">
                                <li><a class="text-center collapsed" data-toggle="collapse" data-target="#navbar-second"><i class="icon-circle-down2"></i></a></li>
                            </ul>

                            <div class="navbar-collapse collapse" id="navbar-second">
                                <ul class="nav navbar-nav">
                                    <li class="active"><a href="add_new_software.php">
                                            <i class="icon-new position-left">
                                            </i> Add New Software</a>
                                    </li>
                                    <li class=""><a href="view_software.php?viewBy=running">
                                            <i class="icon-list-unordered position-left">
                                            </i> All Software</a>
                                    </li>
                                    <li class=""><a href="view_software.php?viewBy=disabled">
                                            <i class="icon-blocked position-left">
                                            </i> Disabled Software</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        
                        <form action="store.php" method="post">
                            <div class="panel-flat">
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <?php if (!empty($_SESSION['success'])) { ?>
                                            <div class="alert alert-success alert-styled-left" style="margin: 0 10px 10px 10px;">
                                            <button type="button" class="close" data-dismiss="alert"><span>&times;</span><span class="sr-only">Close</span></button>
                                            <?php $objsoft->sessionMessage('success');?>
                                            </div>
                                            <?php } ?>
                                            <fieldset>
                                                <legend class="text-semibold">Add New Software</legend>
                                                <div class="form-group">
                                                    <label>Lab Id:</label>
                                                    <select name="lab_id" data-placeholder="Select Lab Title" class="select">
                                                        <option value=""></option> 
                                                        <?php 
                                                        $All_lab_id = $objsoft->labInfo();
                                                        foreach ($All_lab_id as $lab_id) { ?><option value="<?php echo $lab_id['id'];?>" <?php
                                                        if(!empty($_SESSION['v_Lab_no']) && $_SESSION['v_Lab_no']==$lab_id['id']) {
                                                        echo 'selected';unset($_SESSION['v_Lab_no']);}
                                                        ?>><?php echo $lab_id['lab_no'];?></option> 
                                                        <?php } ?>
                                                    </select>
                                                    <label class="validation-error-label"><?php $objsoft->sessionMessage('Lab_no'); ?></label>
                                                </div>
                                                <div class="form-group">
                                                    <label>Software Title:</label>
                                                    <input type="text" name="software_title" class="form-control" placeholder="" value="<?php $objsoft->sessionMessage('v_Software_Title')?>">
                                                    <label class="validation-error-label"><?php $objsoft->sessionMessage('Software_Title')?></label>
                                                </div>
                                            </fieldset>
                                        </div>

                                        <div class="col-md-6">
                                            <fieldset>
                                                <legend class="text-semibold">.</legend>
                                                <div class="form-group">
                                                    <label>Software Version:</label>
                                                    <input type="text" name="software_version" class="form-control" placeholder="" value="<?php $objsoft->sessionMessage('v_Software_Version')?>">
                                                    <label class="validation-error-label"><?php $objsoft->sessionMessage('Software_Version')?></label>
                                                </div>
                                                
                                                <div class="form-group">
                                                    <label>Software Type:</label>
                                                    <input type="text" name="software_type" class="form-control" placeholder="" value="<?php $objsoft->sessionMessage('v_Software_Type')?>">
                                                    <label class="validation-error-label"><?php $objsoft->sessionMessage('Software_Type')?></label>
                                                </div>
                                            </fieldset>
                                        </div>
                                    </div>

                                    <div class="text-right">
                                        <button type="submit" class="btn btn-primary">Submit form <i class="icon-arrow-right14 position-right"></i></button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                <!-- /dashboard content -->

                <!-- Footer -->
                <div class="footer text-muted">
                    &copy; 2016. <a href="#">BASIS Institute of Technology & Management</a> Website <a href="http://www.bitm.org.bd/" target="_blank">BITM.COM</a>
                </div>
                <!-- /footer -->
            </div>
            <!-- /content area -->
        </div>
        <!-- /main content -->
    </div>
    <!-- /page content -->
</div>
<!-- /page container -->
</body>
</html>
