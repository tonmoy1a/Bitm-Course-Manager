<?php
include '../../vendor/autoload.php';
use BitmCourseApp\installed_software\InstalledSoftware;

$objsoft = new InstalledSoftware();

$objsoft->prepare($_POST);
$objsoft->validation();
$objsoft->updateSoftware();
?>
