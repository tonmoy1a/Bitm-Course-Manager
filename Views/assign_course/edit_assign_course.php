<?php
include '../../vendor/autoload.php';

use BitmCourseApp\user\user_login\UserLogin;
use BitmCourseApp\assign_course\AssignCourse;

$objlogin = new UserLogin();
$objass = new AssignCourse();

$loged_user = $objlogin->checkLogin();
$objass->prepare($_GET);
$assigned_data = $objass->showAssignedCourse();
?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>BITM Course Manager</title>
<!-- Global stylesheets -->
<link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
<link rel="shortcut icon" type="image/png" href="../elements/bitm-icon.png"/>
<link href="../assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
<link href="../assets/css/minified/bootstrap.min.css" rel="stylesheet" type="text/css">
<link href="../assets/css/minified/core.min.css" rel="stylesheet" type="text/css">
<link href="../assets/css/minified/components.min.css" rel="stylesheet" type="text/css">
<link href="../assets/css/minified/colors.min.css" rel="stylesheet" type="text/css">
<!-- /global stylesheets -->
<!-- Core JS files -->
<script type="text/javascript" src="../assets/js/plugins/loaders/pace.min.js"></script>
<script type="text/javascript" src="../assets/js/core/libraries/jquery.min.js"></script>
<script type="text/javascript" src="../assets/js/core/libraries/bootstrap.min.js"></script>
<script type="text/javascript" src="../assets/js/plugins/loaders/blockui.min.js"></script>
<!-- /core JS files -->
<!-- Theme JS files -->
<script type="text/javascript" src="../assets/js/plugins/visualization/d3/d3.min.js"></script>
<script type="text/javascript" src="../assets/js/plugins/visualization/d3/d3_tooltip.js"></script>
<script type="text/javascript" src="../assets/js/plugins/forms/styling/switchery.min.js"></script>
<script type="text/javascript" src="../assets/js/plugins/forms/styling/uniform.min.js"></script>
<script type="text/javascript" src="../assets/js/plugins/forms/selects/bootstrap_multiselect.js"></script>
<script type="text/javascript" src="../assets/js/plugins/ui/moment/moment.min.js"></script>
<script type="text/javascript" src="../assets/js/plugins/pickers/daterangepicker.js"></script>
<script type="text/javascript" src="../assets/js/plugins/forms/selects/select2.min.js"></script>
<script type="text/javascript" src="../assets/js/pages/form_layouts.js"></script>

<script type="text/javascript" src="../assets/js/core/app.js"></script>
<script type="text/javascript" src="../assets/js/pages/dashboard.js"></script>

<link rel="stylesheet" type="text/css" href="../datepicker/tcal.css" />
<script type="text/javascript" src="../datepicker/tcal.js"></script>

<script type="text/javascript" src="../timepicker/bootstrap-timepicker.js"></script>
<script type="text/javascript" src="../timepicker/bootstrap-timepicker.min.js"></script>
<!-- /theme JS files -->
</head>

<body>
<!-- Main navbar -->
<div class="navbar navbar-inverse">
    <div class="navbar-header">
        <a class="navbar-brand" href="../dashboard.php"><img src="../assets/images/logo_light.png" alt=""></a>

        <ul class="nav navbar-nav visible-xs-block">
            <li><a data-toggle="collapse" data-target="#navbar-mobile"><i class="icon-tree5"></i></a></li>
            <li><a class="sidebar-mobile-main-toggle"><i class="icon-paragraph-justify3"></i></a></li>
        </ul>
    </div>

    <div class="navbar-collapse collapse" id="navbar-mobile">
        <ul class="nav navbar-nav">
            <li><a class="sidebar-control sidebar-main-toggle hidden-xs"><i class="icon-paragraph-justify3"></i></a></li>
        </ul>

        <ul class="nav navbar-nav navbar-right">
            <li class="dropdown dropdown-user">
                <a class="dropdown-toggle" data-toggle="dropdown">
                    <img src="../elements/user_images/<?php echo $loged_user['image']; ?>" width="28" height="28" alt="">
                    <span><?php echo $loged_user['username']; ?></span>
                    <i class="caret"></i>
                </a>

                <ul class="dropdown-menu dropdown-menu-right">
                    <li><a href="../user/user_view_single.php?id=<?php echo $loged_user['id']; ?>"><i class="icon-user-plus"></i> My profile</a></li>
                    <li class="divider"></li>
                    <li><a href="#"><i class="icon-cog5"></i> Account settings</a></li>
                    <li><a href="../logout.php"><i class="icon-switch2"></i> Logout</a></li>
                </ul>
            </li>
        </ul>
    </div>
</div>
<!-- /main navbar -->
<!-- Page container -->
<div class="page-container">
    <!-- Page content -->
    <div class="page-content">
        <!-- Main sidebar -->
        <div class="sidebar sidebar-main">
            <div class="sidebar-content">
                <!-- User menu -->
                <div class="sidebar-user">
                    <div class="category-content">
                        <div class="media">
                            <a href="#" class="media-left"><img src="../elements/user_images/<?php echo $loged_user['image']; ?>" class="img-circle img-sm" alt=""></a>
                            <div class="media-body">
                                <span class="media-heading text-semibold"><?php echo $loged_user['full_name']; ?></span>
                                <div class="text-size-mini text-muted">

                                </div>
                            </div>

                            <div class="media-right media-middle">
                                <ul class="icons-list">
                                    <li>
                                        <a href="#"><i class="icon-cog3"></i></a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /user menu -->
                <!-- Main navigation -->
                <div class="sidebar-category sidebar-category-visible">
                    <div class="category-content no-padding">
                        <ul class="navigation navigation-main navigation-accordion">

                            <!-- Main -->
                            <li class="navigation-header"><span>Main</span> <i class="icon-menu" title="Main pages"></i></li>
                            <li><a href="../dashboard.php"><i class="icon-home4"></i> <span>Dashboard</span></a></li>
                            <li><a href="../user/user_manager.php"><i class="icon-user"></i> <span>User Menu</span></a></li>
                            <li><a href="../trainer/trainer.php"><i class="icon-people"></i> <span>Trainer</span></a></li>
                            <li><a href="../course/course.php"><i class="icon-book"></i> <span>Course</span></a></li>
                            <li><a href="../lab_info/lab_info.php"><i class="icon-lan"></i> <span>Lab Info</span></a></li>
                            <li><a href="../installed_software/software_info.php"><i class="icon-laptop"></i> <span>Installed Software</span></a></li>
                            <li class="active"><a href="assign_course.php"><i class="icon-list-unordered"></i> <span>Assign Course</span></a></li>
                            <li><a href="../about_project.php"><i class="icon-primitive-square"></i> <span>About Project</span></a></li>
                        </ul>
                    </div>
                </div>
                <!-- /main navigation -->
            </div>
        </div>
        <!-- /main sidebar -->
        <!-- Main content -->
        <div class="content-wrapper">
            <!-- Page header -->
            <div class="page-header">
                <div class="page-header-content">
                    <div class="page-title">
                        <h4><i class="icon-list-unordered"></i> <span class="text-semibold">- Assign Course</span> </h4>
                    </div>
                </div>

                <div class="breadcrumb-line">
                    <ul class="breadcrumb">
                        <li><a href="../dashboard.php"><i class="icon-home2 position-left"></i> Dashboard</a></li>
                        <li class="active">Assign Course</li>
                    </ul>
                </div>

            </div>
            <!-- /page header -->
            <!-- Content area -->
            <div class="content">
                <!-- Dashboard content -->
                <form action="update.php" method="post">
                    <div class="panel panel-flat">
                        <div class="navbar navbar-default navbar-xs">
                                <ul class="nav navbar-nav no-border visible-xs-block">
                                    <li><a class="text-center collapsed" data-toggle="collapse" data-target="#navbar-second"><i class="icon-circle-down2"></i></a></li>
                                </ul>

                                <div class="navbar-collapse collapse" id="navbar-second">
                                    <ul class="nav navbar-nav">
                                        <li class=""><a href="assign_new_course.php">
                                                <i class=" icon-new position-left">
                                                </i> Assign New Course</a>
                                        </li>
                                        <li class=""><a href="search_assigned_course.php">
                                                <i class="icon-search4 position-left">
                                                </i> Search Course</a>
                                        </li>
                                        <li class=""><a href="view_assigned_course.php?viewBy=running">
                                                <i class="icon-list position-left">
                                                </i> All Running Course</a>
                                        </li>
                                        <li class=""><a href="view_assigned_course.php?viewBy=OldCourse">
                                                <i class="icon-file-minus position-left">
                                                </i> Old Course</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        <div class="panel-heading">
                            <h5 class="panel-title">Edit Assigned Course</h5>
                        </div>

                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <?php if (!empty($_SESSION['message'])) { ?>
                                <div class="alert alert-success alert-styled-left" style="margin: 0 10px 10px 10px;">
                                    <button type="button" class="close" data-dismiss="alert"><span>&times;</span><span class="sr-only">Close</span></button>
                                    <?php $objlogin->sessionMessage('message');?>
                                </div>
                                <?php } ?>
                                    <fieldset class="text-semibold">
                                        <legend>Course Info</legend>

                                        <div class="form-group">
                                            <label>Course Title:</label>
                                            <select name="course_title" data-placeholder="Select Course Title" class="select">
                                                <option value=""></option>
                                                <?php
                                                $course_title = $objass->courseTitle();
                                                foreach ($course_title as $all_course) { ?>
                                                <option value="<?php echo $all_course['id'];?>" <?php
                                                if (empty($_SESSION['v_Course_Tilte']) && $assigned_data['course_id']==$all_course['id']) {
                                                    echo 'selected';
                                                } elseif (isset($_SESSION['v_Course_Tilte']) && !empty($_SESSION['v_Course_Tilte']) && $_SESSION['v_Course_Tilte']==$all_course['id']) {
                                                    echo 'selected';
                                                }?>><?php echo $all_course['title'];?></option>
                                                <?php } unset($_SESSION['v_Course_Tilte']); ?>
                                            </select>
                                            <label class="validation-error-label"><?php $objlogin->sessionMessage('Course_Tilte'); ?></label>
                                        </div>

                                        <div class="form-group">
                                            <label>Batch No:</label>
                                            <input type="text" name="batch_no" class="form-control <?php if(!empty($_SESSION['v_Batch_No']) && $_SESSION['v_Batch_No']!==$assigned_data['batch_no']){echo 'text-blue';}?>" placeholder="Add Batch No. (Hint. PHP-26)" value="<?php
                                                                                    if (!empty($_SESSION['v_Batch_No'])) {
                                                                                        $objlogin->sessionMessage('v_Batch_No');
                                                                                    } else {
                                                                                        echo $assigned_data['batch_no'];
                                                                                    } ?>">
                                            <label class="validation-error-label"><?php $objlogin->sessionMessage('Batch_No'); ?></label>
                                        </div>

                                        <div class="form-group">
                                            <label>Lead Trainer:</label>
                                            <select name="lead_trainer" data-placeholder="Select Lead Trainer" class="select">
                                                <option value=""></option>
                                                <?php $All_lead_trainer = $objass->leadTrainer();
                                                foreach ($All_lead_trainer as $lead_trainer) { ?><option <?php
                                                if (empty($_SESSION['v_Lead_Trainer']) && $assigned_data['lead_trainer']==$lead_trainer['full_name']) {
                                                    echo 'selected';
                                                } elseif (isset($_SESSION['v_Lead_Trainer']) && !empty($_SESSION['v_Lead_Trainer']) && $_SESSION['v_Lead_Trainer']==$lead_trainer['full_name']) {
                                                    echo 'selected';
                                                } ?>><?php echo $lead_trainer['full_name'];?></option> 
                                                <?php } unset($_SESSION['v_Lead_Trainer']); ?>
                                            </select>
                                            <label class="validation-error-label"><?php $objlogin->sessionMessage('Lead_Trainer'); ?></label>
                                        </div>

                                        <div class="form-group">
                                            <label>Assistant Trainer:</label>
                                            <select name="assistant_trainer" data-placeholder="Select Assistant Trainer" class="select">
                                                <option value=""></option> 
                                                <?php 
                                                $All_assit_trainer = $objass->assitTrainer();
                                                foreach ($All_assit_trainer as $assit_trainer) { ?>
                                                <option <?php
                                                if (empty($_SESSION['v_Assis_Trainer']) && $assigned_data['asst_trainer']==$assit_trainer['full_name']) {
                                                    echo 'selected';
                                                } elseif (isset($_SESSION['v_Assis_Trainer']) && !empty($_SESSION['v_Assis_Trainer']) && $_SESSION['v_Assis_Trainer']==$assit_trainer['full_name']) {
                                                    echo 'selected';
                                                } ?>><?php echo $assit_trainer['full_name'];?></option> 
                                                <?php } unset($_SESSION['v_Assis_Trainer']); ?>
                                            </select>
                                            <label class="validation-error-label"><?php $objlogin->sessionMessage('Assis_Trainer'); ?></label>
                                        </div>

                                        <div class="form-group">
                                            <label>Lab Assistant:</label>
                                            <select name="lab_assistant" data-placeholder="Select Lab Assistant" class="select">
                                                <option value=""></option> 
                                                <?php 
                                                $All_lab_assit = $objass->labAssit();
                                                foreach ($All_lab_assit as $lab_assit) { ?>
                                                <option <?php
                                                if (empty($_SESSION['v_Lab_Assistant']) && $assigned_data['lab_asst']==$lab_assit['full_name']) {
                                                    echo 'selected';
                                                } elseif (isset($_SESSION['v_Lab_Assistant']) && !empty($_SESSION['v_Lab_Assistant']) && $_SESSION['v_Lab_Assistant']==$lab_assit['full_name']) {
                                                    echo 'selected';
                                                } ?>><?php echo $lab_assit['full_name'];?></option> 
                                                <?php } unset($_SESSION['v_Lab_Assistant']); ?>
                                            </select>
                                            <label class="validation-error-label"><?php $objlogin->sessionMessage('Lab_Assistant'); ?></label>
                                        </div>

                                        <div class="form-group">
                                            <label>Lab Id:</label>
                                            <select name="Lab_Id" data-placeholder="Select Lab Title" class="select">
                                                <option value=""></option> 
                                                <?php 
                                                $All_lab_id = $objass->labInfo();
                                                foreach ($All_lab_id as $lab_id) { ?>
                                                <option <?php
                                                if (empty($_SESSION['v_Lab_Id']) && $assigned_data['lab_id']==$lab_id['lab_no']) {
                                                    echo 'selected';
                                                } elseif (isset($_SESSION['v_Lab_Id']) && !empty($_SESSION['v_Lab_Id']) && $_SESSION['v_Lab_Id']==$lab_id['lab_no']) {
                                                    echo 'selected';
                                                } ?>><?php echo $lab_id['lab_no'];?></option> 
                                                <?php } unset($_SESSION['v_Lab_Id']); ?>
                                            </select>
                                            <label class="validation-error-label"><?php $objlogin->sessionMessage('Lab_Id'); ?></label>
                                        </div>
                                    </fieldset>
                                </div>

                                <div class="col-md-6">
                                    <fieldset>
                                        <legend class="text-semibold">Date and Time</legend>
                                        <label class="validation-error-label"><?php 
                                        $objlogin->sessionMessage('time_match'); 
                                        if (!empty($_SESSION['match_id'])) {
                                            echo '<br/><a href="view_assigned_course_single.php?id='.$_SESSION['match_id'].'" target="_blank">View Matched Course</a>';
                                            unset($_SESSION['match_id']);
                                        }
                                        ?></label>
                                        <div class="form-group">
                                            <label>Day:</label>
                                            
                                            <select name="day_session" data-placeholder="Select Day" class="select">
                                                <option value=""></option> 
                                                <option value="sess-1" <?php if(!empty($_SESSION['v_Day']) && $_SESSION['v_Day']=='sess-1'){echo 'selected';unset($_SESSION['v_Day']);
                                                    } elseif (empty($_SESSION['v_Day']) && $assigned_data['day']=='sess-1') {echo 'selected'; } ?>>Saturday ,Monday ,Wednesday</option> 
                                                <option value="sess-2" <?php if(!empty($_SESSION['v_Day']) && $_SESSION['v_Day']=='sess-2'){echo 'selected';unset($_SESSION['v_Day']);
                                                    } elseif (empty($_SESSION['v_Day']) && $assigned_data['day']=='sess-2') {echo 'selected'; } ?>>Sunday, Tuesday, Thursday</option>
                                                <option value="sess-3" <?php if(!empty($_SESSION['v_Day']) && $_SESSION['v_Day']=='sess-3'){echo 'selected';unset($_SESSION['v_Day']);
                                                    } elseif (empty($_SESSION['v_Day']) && $assigned_data['day']=='sess-3') {echo 'selected';} ?>>Friday</option>
                                            </select>
                                            <label class="validation-error-label"><?php $objlogin->sessionMessage('Day'); ?></label>
                                        </div>
                                        <div class="form-group">
                                            <label>Start Date:</label>
                                            <input type="text" name="start_date" class="form-control tcal <?php if(!empty($_SESSION['v_Start_Date']) && $_SESSION['v_Start_Date']!==$assigned_data['start_date']){echo 'text-blue';}?>" placeholder="Select Start Date" value="<?php
                                                                                    if (!empty($_SESSION['v_Start_Date'])) {
                                                                                        $objlogin->sessionMessage('v_Start_Date');
                                                                                    } else {
                                                                                        echo $assigned_data['start_date'];
                                                                                    } ?>">
                                            <label class="validation-error-label"><?php $objlogin->sessionMessage('Start_Date'); ?></label>
                                        </div>
                                        <label class="validation-error-label"><?php $objlogin->sessionMessage('inv_date'); ?></label>
                                        <div class="form-group">
                                            <label>Ending Date:</label>
                                            <input type="text" name="end_date" class="form-control tcal <?php if(!empty($_SESSION['v_End_Date']) && $_SESSION['v_End_Date']!==$assigned_data['ending_date']){echo 'text-blue';}?>" placeholder="Select Ending Date" value="<?php
                                                                                    if (!empty($_SESSION['v_End_Date'])) {
                                                                                        $objlogin->sessionMessage('v_End_Date');
                                                                                    } else {
                                                                                        echo $assigned_data['ending_date'];
                                                                                    } ?>">
                                            <label class="validation-error-label"><?php $objlogin->sessionMessage('End_Date'); ?></label>
                                        </div>
                                        <div class="form-group">
                                            <label>Start Time:</label>
                                            <input type="text" id="timepicker5" name="start_time" class="form-control <?php if(!empty($_SESSION['v_Start_Time']) && $_SESSION['v_Start_Time']!==$assigned_data['start_time']){echo 'text-blue';}?>" placeholder="Select Start Time" value="<?php
                                                                                    if (!empty($_SESSION['v_Start_Time'])) {
                                                                                        $objlogin->sessionMessage('v_Start_Time');
                                                                                    } else {
                                                                                        echo $assigned_data['start_time'];
                                                                                    } ?>">
                                            <label class="validation-error-label"><?php $objlogin->sessionMessage('Start_Time'); ?></label>
                                        </div>
                                        <label class="validation-error-label"><?php $objlogin->sessionMessage('inv_time'); ?></label>
                                        <div class="form-group">
                                            <label>Ending Time:</label>
                                            <input type="text" id="timepicker6" name="end_time" class="form-control <?php if(!empty($_SESSION['v_End_Time']) && $_SESSION['v_End_Time']!==$assigned_data['ending_time']){echo 'text-blue';}?>" placeholder="Select Ending Time" value="<?php $objlogin->sessionMessage('v_End_Time'); ?><?php
                                                                                    if (!empty($_SESSION['v_End_Time'])) {
                                                                                        $objlogin->sessionMessage('v_End_Time');
                                                                                    } else {
                                                                                        echo $assigned_data['ending_time'];
                                                                                    } ?>">
                                            <label class="validation-error-label"><?php $objlogin->sessionMessage('End_Time'); ?></label>
                                        </div>
                                    </fieldset>
                                </div>
                            </div>
                            <input type="hidden" name="id" value="<?php echo $assigned_data['id'];?>"/>
                            <div class="text-right">
                                <button type="submit" class="btn btn-primary">Submit form <i class="icon-arrow-right14 position-right"></i></button>
                            </div>
                        </div>
                    </div>
                </form>
                <!-- /dashboard content -->
                <script type="text/javascript">
                    $('#timepicker5').timepicker({
                        template: false,
                        showInputs: false,
                        minuteStep: 5
                    });
                    $('#timepicker6').timepicker({
                        template: false,
                        showInputs: false,
                        minuteStep: 5
                    });
                </script>

                <!-- Footer -->
                <div class="footer text-muted">
                    &copy; 2016. <a href="#">BASIS Institute of Technology & Management</a> Website <a href="http://www.bitm.org.bd/" target="_blank">BITM.COM</a>
                </div>
                <!-- /footer -->
            </div>
            <!-- /content area -->
        </div>
        <!-- /main content -->
    </div>
    <!-- /page content -->
</div>
<!-- /page container -->
</body>
</html>
