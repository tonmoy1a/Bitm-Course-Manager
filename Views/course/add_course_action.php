<?php
include '../../vendor/autoload.php';
use BitmCourseApp\course\Course;
$objass = new Course();

$objass->prepare($_POST);
$objass->assignCourseValidation();
$objass->store();