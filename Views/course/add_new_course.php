<?php
include '../../vendor/autoload.php';

use BitmCourseApp\user\user_login\UserLogin;

$objlogin = new UserLogin();
$loged_user = $objlogin->checkLogin();
?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>BITM Course Manager</title>
<!-- Global stylesheets -->
<link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
<link rel="shortcut icon" type="image/png" href="../elements/bitm-icon.png"/>
<link href="../assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
<link href="../assets/css/minified/bootstrap.min.css" rel="stylesheet" type="text/css">
<link href="../assets/css/minified/core.min.css" rel="stylesheet" type="text/css">
<link href="../assets/css/minified/components.min.css" rel="stylesheet" type="text/css">
<link href="../assets/css/minified/colors.min.css" rel="stylesheet" type="text/css">
<!-- /global stylesheets -->
<!-- Core JS files -->
<script type="text/javascript" src="../assets/js/plugins/loaders/pace.min.js"></script>
<script type="text/javascript" src="../assets/js/core/libraries/jquery.min.js"></script>
<script type="text/javascript" src="../assets/js/core/libraries/bootstrap.min.js"></script>
<script type="text/javascript" src="../assets/js/plugins/loaders/blockui.min.js"></script>
<!-- /core JS files -->
<!-- Theme JS files -->
<script type="text/javascript" src="../assets/js/plugins/visualization/d3/d3.min.js"></script>
<script type="text/javascript" src="../assets/js/plugins/visualization/d3/d3_tooltip.js"></script>
<script type="text/javascript" src="../assets/js/plugins/forms/styling/switchery.min.js"></script>
<script type="text/javascript" src="../assets/js/plugins/forms/styling/uniform.min.js"></script>
<script type="text/javascript" src="../assets/js/plugins/forms/selects/bootstrap_multiselect.js"></script>
<script type="text/javascript" src="../assets/js/plugins/ui/moment/moment.min.js"></script>
<script type="text/javascript" src="../assets/js/plugins/pickers/daterangepicker.js"></script>
<script type="text/javascript" src="../assets/js/plugins/forms/selects/select2.min.js"></script>
<script type="text/javascript" src="../assets/js/pages/form_layouts.js"></script>

<script type="text/javascript" src="../assets/js/core/app.js"></script>
<script type="text/javascript" src="../assets/js/pages/dashboard.js"></script>
<!-- /theme JS files -->
</head>

<body>
<!-- Main navbar -->
<div class="navbar navbar-inverse">
    <div class="navbar-header">
        <a class="navbar-brand" href="../dashboard.php"><img src="../assets/images/logo_light.png" alt=""></a>
        <ul class="nav navbar-nav visible-xs-block">
            <li><a data-toggle="collapse" data-target="#navbar-mobile"><i class="icon-tree5"></i></a></li>
            <li><a class="sidebar-mobile-main-toggle"><i class="icon-paragraph-justify3"></i></a></li>
        </ul>
    </div>

    <div class="navbar-collapse collapse" id="navbar-mobile">
        <ul class="nav navbar-nav">
            <li><a class="sidebar-control sidebar-main-toggle hidden-xs"><i class="icon-paragraph-justify3"></i></a></li>
        </ul>
        <ul class="nav navbar-nav navbar-right">
            <li class="dropdown dropdown-user">
                <a class="dropdown-toggle" data-toggle="dropdown">
                    <img src="../elements/user_images/<?php echo $loged_user['image']; ?>" width="28" height="28" alt="">
                    <span><?php echo $loged_user['username']; ?></span>
                    <i class="caret"></i>
                </a>
                <ul class="dropdown-menu dropdown-menu-right">
                    <li><a href="../user/user_view_single.php?id=<?php echo $loged_user['id']; ?>"><i class="icon-user-plus"></i> My profile</a></li>
                    <li class="divider"></li>
                    <li><a href="#"><i class="icon-cog5"></i> Account settings</a></li>
                    <li><a href="../logout.php"><i class="icon-switch2"></i> Logout</a></li>
                </ul>
            </li>
        </ul>
    </div>
</div>
<!-- /main navbar -->
<!-- Page container -->
<div class="page-container">
    <!-- Page content -->
    <div class="page-content">
        <!-- Main sidebar -->
        <div class="sidebar sidebar-main">
            <div class="sidebar-content">
                <!-- User menu -->
                <div class="sidebar-user">
                    <div class="category-content">
                        <div class="media">
                            <a href="#" class="media-left"><img src="../elements/user_images/<?php echo $loged_user['image']; ?>" class="img-circle img-sm" alt=""></a>
                            <div class="media-body">
                                <span class="media-heading text-semibold"><?php echo $loged_user['full_name']; ?></span>
                                <div class="text-size-mini text-muted">

                                </div>
                            </div>

                            <div class="media-right media-middle">
                                <ul class="icons-list">
                                    <li>
                                        <a href="#"><i class="icon-cog3"></i></a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /user menu -->
                <!-- Main navigation -->
                <div class="sidebar-category sidebar-category-visible">
                    <div class="category-content no-padding">
                        <ul class="navigation navigation-main navigation-accordion">
                            <!-- Main -->
                            <li class="navigation-header"><span>Main</span> <i class="icon-menu" title="Main pages"></i></li>
                            <li><a href="../dashboard.php"><i class="icon-home4"></i> <span>Dashboard</span></a></li>
                            <li><a href="../user/user_manager.php"><i class="icon-user"></i> <span>User Menu</span></a></li>
                            <li><a href="../trainer/trainer.php"><i class="icon-people"></i> <span>Trainer</span></a></li>
                            <li class="active"><a href="../course/course.php"><i class="icon-book"></i> <span>Course</span></a></li>
                            <li><a href="../lab_info/lab_info.php"><i class="icon-lan"></i> <span>Lab Info</span></a></li>
                            <li><a href="../installed_software/software_info.php"><i class="icon-laptop"></i> <span>Installed Software</span></a></li>
                            <li><a href="../assign_course/assign_course.php"><i class="icon-list-unordered"></i> <span>Assign Course</span></a></li>
                        </ul>
                    </div>
                </div>
                <!-- /main navigation -->
            </div>
        </div>
        <!-- /main sidebar -->
        <!-- Main content -->
        <div class="content-wrapper">
            <!-- Page header -->
            <div class="page-header">
                <div class="page-header-content">
                    <div class="page-title">
                        <h4><i class="icon-lan"></i> - <span class="text-semibold">Course Information</span></h4>
                    </div>
                </div>

                <div class="breadcrumb-line">
                    <ul class="breadcrumb">
                        <li><a href="../dashboard.php"><i class="icon-home2 position-left"></i> Dashboard</a></li>
                        <li class="active">Course Information</li>
                    </ul>
                </div>
            </div>
            <!-- /page header -->
            <!-- Content area -->
            <div class="content">
                <!-- Dashboard content -->
                    <div class="panel panel-flat">
                        <div class="navbar navbar-default navbar-xs">
                            <ul class="nav navbar-nav no-border visible-xs-block">
                                <li><a class="text-center collapsed" data-toggle="collapse" data-target="#navbar-second"><i class="icon-circle-down2"></i></a></li>
                            </ul>

                            <div class="navbar-collapse collapse" id="navbar-second">
                                <ul class="nav navbar-nav">
                                    <li class=""><a href="add_new_course.php">
                                        <i class="icon-new position-left text-teal-800">
                                        </i> Add New Course</a>
                                    </li>
                                    <li class=""><a href="view_course.php?viewBy=running">
                                        <i class="icon-list-unordered position-left text-teal-800">
                                        </i> View All Course</a>
                                    </li>
                                    <li class=""><a href="view_course.php?viewBy=disabled">
                                        <i class="icon-blocked position-left text-teal-800">
                                        </i> View Disabled Course</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                            <div class="col-lg-12">
                                <div class="panel registration-form">
                                    <div class="panel-body">
                                        <div class="text-center">
                                            <div class="icon-object border-teal text-teal"><i class="icon-graduation2"></i>
                                            </div>
                                            <h5 class="content-group-lg">Add New Course</h5>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <!-- Horizontal form -->
                                                <div class="panel panel-flat">
                                                    <div class="panel-body">

                                                    <form action="add_course_action.php" class="form-horizontal course" method="POST">
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label class="control-label col-lg-3">Course Name</label>
                                                                    <div class="col-lg-9">
                                                                        <input type="text" class="form-control" placeholder="Provide Course Name" name="title" value="">
                                                                                                                        </div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label class="control-label col-lg-3">Course type</label>
                                                                    <div class="col-lg-9">
                                                                        <label class="radio-inline">
                                                                        <input type="radio" class="styled" name="course_type" value="paid" checked="checked">
                                                                        Paid Course
                                                                    </label>

                                                                    <label class="radio-inline">
                                                                        <input type="radio" class="styled" name="course_type" value="free" id="free_course">
                                                                        Free Course
                                                                    </label>
                                                                                                                    </div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label class="control-label col-lg-3">Course Offer</label>
                                                                    <div class="col-lg-9">
                                                                        <label class="checkbox-inline">
                                                                        <input type="checkbox" name="is_offer" value="1">
                                                                        Offered Course
                                                                        </label>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">

                                                                <div class="form-group">
                                                                    <label class="control-label col-lg-3">Duration</label>
                                                                    <div class="col-lg-9">
                                                                        <select class="bootstrap-select bs-select-hidden" data-width="100%" name="duration">
                                                                        <option></option>
                                                                        <option value="1_month">One Month</option>
                                                                        <option value="2_months">Two Month</option>
                                                                        <option value="3_months">Three Month</option>

                                                                        </select><div class="btn-group bootstrap-select dropup" style="width: 100%;"><button type="button" class="btn dropdown-toggle btn-default" data-toggle="dropdown" title="One Month" aria-expanded="false"><span class="filter-option pull-left">One Month</span>&nbsp;<span class="caret"></span></button><div class="dropdown-menu open active" style="max-height: 381px; overflow: hidden; min-height: 122px;"><ul class="dropdown-menu inner" role="menu" style="max-height: 369px; overflow-y: auto; min-height: 110px;"><li data-original-index="0" class=""><a tabindex="0" class="" style="" data-tokens="null"><span class="text"></span><span class=" icon-checkmark3 check-mark"></span></a></li><li data-original-index="1" class="active"><a tabindex="0" class="" style="" data-tokens="null"><span class="text">One Month</span><span class=" icon-checkmark3 check-mark"></span></a></li><li data-original-index="2"><a tabindex="0" class="" style="" data-tokens="null"><span class="text">Two Month</span><span class=" icon-checkmark3 check-mark"></span></a></li><li data-original-index="3"><a tabindex="0" class="" style="" data-tokens="null"><span class="text">Three Month</span><span class=" icon-checkmark3 check-mark"></span></a></li></ul></div></div>
                                                                                                                        </div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label class="control-label col-lg-3">Course Price</label>
                                                                    <div class="col-lg-9">
                                                                       <div class="input-group">
                                                                         <input type="number" class="form-control" placeholder="Provide Course Price" name="course_fee" value="" id="course_price">
                                                                         <span class="input-group-addon">Taka</span>
                                                                       </div>
                                                                                                                        </div>
                                                                </div>

                                                                <div class="form-group">
                                                                    <label class="control-label col-lg-3">Course Description</label>
                                                                    <div class="col-lg-9">
                                                                        <textarea rows="5" cols="5" class="form-control" name="description" placeholder="Write description about that Courses"></textarea>
                                                                                                                        </div>
                                                                </div>


                                                            </div>
                                                            <div class="text-right">
                                                                <button class="btn bg-teal" type="submit">Create Course<i class="icon-arrow-right14 position-right"></i></button>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            </div>
                        </div>
                    </div>
                <!-- /dashboard content -->
                <!-- Footer -->
                <div class="footer text-muted">
                    &copy; 2016. <a href="#">BASIS Institute of Technology & Management</a> Website <a href="http://www.bitm.org.bd/" target="_blank">BITM.COM</a>
                </div>
                <!-- /footer -->
            </div>
            <!-- /content area -->
        </div>
        <!-- /main content -->
    </div>
    <!-- /page content -->
</div>
<!-- /page container -->
</body>
</html>